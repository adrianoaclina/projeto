package br.com.ozcorp;

public class FuncionarioTesteDrive{

	public static void main(String[] args) {
		
		Cargo engenheiro = new Cargo("Engenheiro", 5_000.00);
		Cargo analista = new Cargo("Analista", 10_000.00);
		Cargo gerente = new Cargo("Gerente administrativo", 5_000.00);
		Cargo diretor = new Cargo("Diretor financeiro", 2_500.00);
		Cargo secretario = new Cargo("Secretario", 3_000.00);
		
		Departamento financeiro = new Departamento("Financeiro", "Fin", diretor);
		Departamento administrativo = new Departamento("Administrativo", "ADM", gerente);
		Departamento tecnicodeinformatica = new Departamento("T�cnico de Inform�tica", "TI", analista);
		Departamento trabalhista = new Departamento("Trabalhista", "TI", engenheiro);
		Departamento secretaria = new Departamento("Secretaria", "SEC", secretario);
		
		Funcionario adriano = new Funcionario("Adriano", "543835121", "47533804830", "501450", "adriano.aclina@gmail.com", "123", Sexo.MASCULINO, TipoSanguineo.A_NEGATIVO, 0, financeiro);
	
		System.out.println("Dados do funcion�rio: ");
		System.out.println("Nome            : " + adriano.getNome());
		System.out.println("RG              : " + adriano.getRg());
		System.out.println("CPF             : " + adriano.getCpf());
		System.out.println("Matr�cula       : " + adriano.getMatricula());
		System.out.println("E-Mail          : " + adriano.getEmail());
		System.out.println("Senha           : " + adriano.getSenha());
		System.out.println("Cargo           : " + adriano.getDepartamento().getCargo().getTitulo());
		System.out.println("Sal�rio base    : " + adriano.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo sangu�neo  : " + adriano.getTipoSanguineo());
		System.out.println("Sexo            : " + adriano.getSexo());
	}
}