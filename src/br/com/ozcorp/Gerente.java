package br.com.ozcorp;

public class Gerente extends Funcionario {

	public Gerente(String nome, String rg, String cpf, String matricula, String email, String senha, Sexo sexo,
			TipoSanguineo tipoSanguineo, int nivelAcesso, Departamento departamento) {
		super(nome, rg, cpf, matricula, email, senha, sexo, tipoSanguineo, nivelAcesso, departamento);
		
	}

}
